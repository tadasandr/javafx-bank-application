package controllers;

import singleton.UsersSingleton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

import java.io.IOException;

public class LoginController {

    public ScreenController screenController = ScreenController.getInstance();
    public UsersSingleton us = UsersSingleton.getInstance();

    @FXML
    public PasswordField password;

    @FXML
    public TextField username;

    @FXML
    public Label errorLabel;

    @FXML
    public void handleLogin() throws IOException {
        boolean areCredentialsValid = us.login(username.getText(), password.getText());
        if(areCredentialsValid) {
            System.out.println("SUCCESSFUL LOGIN");
            screenController.addScreen("dashboard", FXMLLoader.load(getClass().getClassLoader()
                    .getResource("views/dashboard.fxml")));
            screenController.activate("dashboard");
        } else {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Invalid credentials");
        }
    }

    @FXML
    public void navigateToRegister() {
        screenController.activate("register");
    }
}
