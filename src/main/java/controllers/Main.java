package controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class Main extends Application {

    public static Stage mainStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("views/login.fxml"));
        primaryStage.setTitle("KVK Bank");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 900, 500));
        primaryStage.show();

        setMainStage(primaryStage);

        ScreenController screenController = ScreenController.getInstance();
        screenController.setMainScene(primaryStage.getScene());
        screenController.addScreen("login", FXMLLoader.load(getClass().getClassLoader().getResource("views/login.fxml")));
        screenController.addScreen("register", FXMLLoader.load(getClass().getClassLoader().getResource("views/register.fxml")));
    }

    public static Stage getMainStage() {
        return mainStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
