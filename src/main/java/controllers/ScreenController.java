package controllers;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;

import java.util.HashMap;

public class ScreenController {
    private final static ScreenController instance = new ScreenController();
    private HashMap<String, Pane> screenMap = new HashMap<>();
    private Scene main;

    private ScreenController() {
    }

    public static ScreenController getInstance() {
        return instance;
    }

    public void addScreen(String name, Pane pane){
        screenMap.put(name, pane);
    }

    public void setMainScene(Scene scene){
        this.main = scene;
    }

    public void removeScreen(String name){
        screenMap.remove(name);
    }

    public void activate(String name){
        main.setRoot(screenMap.get(name));
    }

}
