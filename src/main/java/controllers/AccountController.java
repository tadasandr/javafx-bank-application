package controllers;

import builder.Payment;
import command.*;
import facade.ChangePasswordFacade;
import facade.LogoutFacade;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import singleton.Store;
import singleton.UsersSingleton;
import strategy.DataManagerStrategy;
import strategy.IncomeDataManager;
import utility.Clean;
import utility.DateFormat;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

public class AccountController {

    final ScreenController screenController = ScreenController.getInstance();
    final DataManagerStrategy idm = IncomeDataManager.getInstance();
    final UsersSingleton us = UsersSingleton.getInstance();

    @FXML
    public ListView<Payment> incomeList;

    @FXML
    public Label accountNumber;

    @FXML
    public Label moneySpent;

    @FXML
    public BorderPane mainPane;

    @FXML
    public Label username;

    @FXML
    public PasswordField oldPassword;

    @FXML
    public PasswordField newPassword;

    @FXML
    public PasswordField newPasswordRepeat;

    @FXML
    public Label passwordChangeLabel;

    @FXML
    public void handleNavigationToDashboard() {
        screenController.activate("dashboard");
    }

    public void initialize() {
        moneySpent.setText(us.getLoggedInUser().getId());
        username.setText(us.getLoggedInUser().getUsername());
        Collections.reverse(idm.getHistoryList());
        incomeList.setItems(idm.getHistoryList());
        idm.getHistoryList().forEach(m -> System.out.println(m.toString()));


        Tooltip passwordTooltip = new Tooltip();
        passwordTooltip.setText("Your password must contain:\n" +
                "a digit\n" +
                "a lower case letter\n" +
                "an upper case letter\n" +
                "a special character\n" +
                "white spaces aren't allowed\n" +
                "at least 8 characters and at most 20 characters");
        newPassword.setTooltip(passwordTooltip);
        newPasswordRepeat.setTooltip(passwordTooltip);

        Tooltip clipboardTip = new Tooltip();
        clipboardTip.setText("Right click to copy");
        moneySpent.setTooltip(clipboardTip);

        ContextMenu accountNumberContextMenu = new ContextMenu();
        MenuItem copyToClipboard = new MenuItem("Copy to clipboard");
        accountNumberContextMenu.getItems().addAll(copyToClipboard);
        copyToClipboard.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Clipboard clipboard = Clipboard.getSystemClipboard();
                ClipboardContent content = new ClipboardContent();
                content.putString(moneySpent.getText());
                clipboard.setContent(content);
            }
        });

        moneySpent.setContextMenu(accountNumberContextMenu);

        Tooltip tooltip = new Tooltip();
        tooltip.setText("Double click for more information");
        incomeList.setCellFactory(new Callback<ListView<Payment>, ListCell<Payment>>() {
            @Override
            public ListCell<Payment> call(ListView<Payment> paymentListView) {
                ListCell<Payment> cell = new ListCell<Payment>() {
                    @Override
                    protected void updateItem(Payment item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                        } else {
                            if(us.getLoggedInUser().getId().equals(item.getAccountId())) {
                                setText(String.format("Top-up of %s€", item.getAmount()));
                            } else {
                                setText(String.format("%s€ received from %s", item.getAmount(), item.getAccountId()));
                            }
                            setTooltip(tooltip);
                            setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent event) {
                                    if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                                        try {
                                            showInformationDialog(getItem());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });
                        }
                    }
                };
                return cell;
            }
        });
    }

    @FXML
    private void handlePasswordChange() {
        boolean changeSuccess = ChangePasswordFacade.changePassword(oldPassword.getText(),
                newPassword.getText(), newPasswordRepeat.getText());
        if (changeSuccess) {
            passwordChangeLabel.setTextFill(Color.GREEN);
            passwordChangeLabel.setText("Password changed successfully!");
            Clean.clearLabel(passwordChangeLabel, 5000);
        } else {
            passwordChangeLabel.setTextFill(Color.RED);
            passwordChangeLabel.setText("Passwords do not match or invalid password.");
            Clean.clearLabel(passwordChangeLabel, 5000);
        }
    }

    private void showInformationDialog(Payment payment) throws IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(mainPane.getScene().getWindow());
        alert.setTitle("KVK Bank");
        alert.setHeaderText("Payment information");
        alert.setContentText("Sender's account number:  " + payment.getAccountId() + "\n" +
                "Money amount:                  " + payment.getAmount() + "€" + "\n" +
                "Payment description:          " + payment.getDescription() + "\n" +
                "PaymentID:                         " + payment.getPaymentId() + "\n" +
                "Payment date:                    " + payment.getTransactionDate().format(DateFormat.getFormat()));
        alert.showAndWait();
    }

    @FXML
    private void showTopUp() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Top-up");
        dialog.setHeaderText("Add money to your account");
        dialog.setContentText("Enter money amount: ");

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(s -> {
            CommandController controller = new CommandController();

            UserUpdater topup = new UserUpdater(us.getLoggedInUser(), Float.parseFloat(result.get()));

            Command receiveMoney = new ReceiveMoneyCommand(topup);

            controller.setCommand(receiveMoney);
            controller.execute();

            us.getLoggedInUser().setCashBalance(us.getLoggedInUser().getCashBalance() + Float.parseFloat(result.get()));
            Store store = Store.getInstance();
            store.getBalanceLabel().setText(String.format("Current balance: %s €", us.getLoggedInUser().getCashBalance()));
            Payment payment = new Payment.PaymentBuilder("Some other bank", Float.parseFloat(result.get())).setDescription("Money received from user's other bank account")
                    .setPaymentId("no payment id").setDate(LocalDateTime.parse(LocalDateTime.now().toString())).build();
            idm.addPayment(payment);
            try {
                idm.store(String.format("%s~%s~Money received from user's other bank account~no payment id~%s~%s",
                        "Some other bank", result.get(), LocalDateTime.now(), us.getLoggedInUser().getId()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    public void logout() {
        Platform.exit();
        System.exit(0);
    }
}
