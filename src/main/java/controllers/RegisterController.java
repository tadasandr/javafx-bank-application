package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import proxy.Register;
import proxy.UserRegistrationProxy;

public class RegisterController {

    public ScreenController screenController = ScreenController.getInstance();
    public Register register = new UserRegistrationProxy();
    public boolean isRegistered = false;

    @FXML
    public TextField username;

    @FXML
    public PasswordField password;

    @FXML
    public PasswordField password1;

    @FXML
    public Label errorLabel;

    public void initialize() {
        Tooltip tooltip = new Tooltip();
        tooltip.setText("Your password must contain:\n" +
                "a digit\n" +
                "a lower case letter\n" +
                "an upper case letter\n" +
                "a special character\n" +
                "white spaces aren't allowed\n" +
                "at least 8 characters and at most 20 characters");
        password.setTooltip(tooltip);
        password1.setTooltip(tooltip);
    }

    @FXML
    public void registerUser() throws Exception {
        if (password.getText().isEmpty() || password1.getText().isEmpty() || username.getText().isEmpty()) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Please fill all the fields.");
            return;
        }
        if (!password.getText().equals(password1.getText())) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Passwords don't match!");
            return;
        }
        if (isRegistered) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("You have already registered, please log in");
            return;
        }
        boolean registrationStatus = register.register(username.getText(), password.getText());
        if (!registrationStatus) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Your password does not match the given pattern." +
                    " Hover mouse cursor on any password field for instructions.");
            return;
        }
        errorLabel.setTextFill(Color.GREEN);
        errorLabel.setText("Registration successful, you can log in now.");
        isRegistered = true;
    }

    @FXML
    public void navigateToLogin() {
        screenController.activate("login");
    }
}
