package controllers;

import adapter.*;
import adapter.Currency;
import builder.Payment;
import command.*;
import facade.LogoutFacade;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import singleton.Store;
import strategy.ContextLoader;
import strategy.DataManagerStrategy;
import strategy.IncomeDataManager;
import strategy.SpendingsDataManager;
import datamodel.User;
import singleton.UsersSingleton;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import observer.Observer;
import observer.Subject;
import observer.UserSubject;
import utility.Clean;
import utility.DateFormat;
import utility.Numbers;

import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.*;

public class DashboardController {

    final User user = UsersSingleton.getInstance().getLoggedInUser();
    final DataManagerStrategy sdm = SpendingsDataManager.getInstance();
    final DataManagerStrategy idm = IncomeDataManager.getInstance();
    final UsersSingleton us = UsersSingleton.getInstance();
    final ScreenController screenController = ScreenController.getInstance();
    final Store appStore = Store.getInstance();

    @FXML
    public TextField accountNumber;

    @FXML
    public TextField amount;

    @FXML
    public TextArea description;

    @FXML
    public TextField paymentId;

    @FXML
    public Label balance;

    @FXML
    public Label username;

    @FXML
    Label errorLabel;

    @FXML
    ListView<Payment> spendingsList;

    @FXML
    public BorderPane mainPane;

    private Currency balanceEur;

    public void initialize() throws IOException {

        // TODO: strategy design pattern
        // load spendings history data from files to memory
        ContextLoader loader = new ContextLoader(sdm);
        loader.loadData();

        // initialize balance label


        Store store = Store.getInstance();
        store.setBalanceLabel(balance);

        // main (eur) currency initialization

        this.balanceEur = new EUR(user.getCashBalance());

        // existing adapters initialization and addition to list

        List<CurrencyAdapter> existingCurrencyAdapters = new ArrayList<>() {{
            add(new ConvertEURToEUR(balanceEur));
            add(new ConvertEURToUSD(balanceEur));
            add(new ConvertEURToZL(balanceEur));
        }};

        ContextMenu currencyMenu = new ContextMenu();
        for (CurrencyAdapter adapter : existingCurrencyAdapters) {
            MenuItem currencyItem = new MenuItem(adapter.getName());
            currencyItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    balance.setText("Current balance: " + new DecimalFormat("##.##")
                            .format(adapter.getValue()) + " " + adapter.getCurrencySymbol());
                }
            });
            currencyMenu.getItems().add(currencyItem);
        }

        balance.setContextMenu(currencyMenu);

        // initialize Accounts tab
        screenController.addScreen("account",
                FXMLLoader.load(getClass().getClassLoader().getResource("views/account.fxml")));

        accountNumber.setPromptText("REQUIRED. account number e.g LT54861487951234567");
        initializeBalance();
        amount.setPromptText("REQUIRED. amount e.g 56.42");
        description.setPromptText("NOT REQUIRED. Some additional information about this transaction");
        paymentId.setPromptText("NOT REQUIRED. Additional payment id to identify payment");
        username.setText(" " + user.getUsername());
        Collections.reverse(sdm.getHistoryList());
        spendingsList.setItems(sdm.getHistoryList());

        Tooltip tooltip = new Tooltip();
        tooltip.setText("Double click for more information");
        spendingsList.setCellFactory(new Callback<ListView<Payment>, ListCell<Payment>>() {
                                         @Override
                                         public ListCell<Payment> call(ListView<Payment> paymentListView) {
                                             ListCell<Payment> cell = new ListCell<Payment>() {
                                                 @Override
                                                 protected void updateItem(Payment item, boolean empty) {
                                                     super.updateItem(item, empty);
                                                     if (empty) {
                                                         setText(null);
                                                     } else {
                                                         setTooltip(tooltip);
                                                         setText(String.format("Transferred %s € to %s", item.getAmount(), item.getAccountId()));
                                                         setOnMouseClicked(new EventHandler<MouseEvent>() {
                                                             @Override
                                                             public void handle(MouseEvent event) {
                                                                 if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                                                                     try {
                                                                         showInformationDialog(getItem());
                                                                     } catch (IOException e) {
                                                                         e.printStackTrace();
                                                                     }
                                                                 }
                                                             }
                                                         });
                                                     }
                                                 }
                                             };
                                             return cell;
                                         }
                                     }
        );
    }

    public void initializeBalance() {
        balance.setText(String.format("Current balance: %s €", user.getCashBalance()));
    }

    @FXML
    public void handlePayment() throws IOException {
        Payment payment;
        User receiver = us.findById(accountNumber.getText());
        if (receiver == null) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Receiver's account number doesn't exist");
            Clean.clearLabel(errorLabel, 5000);
            return;
        }
        if (receiver.getId().equals(user.getId())) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Can't send money to yourself :(");
            Clean.clearLabel(errorLabel, 5000);
            return;
        }
        if (!Numbers.isNumeric(amount.getText())) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Amount must be a number!");
            Clean.clearLabel(errorLabel, 5000);
            return;
        }
        if (user.getCashBalance() < Float.parseFloat(amount.getText())) {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Insufficient funds");
            Clean.clearLabel(errorLabel, 5000);
            return;
        }
        if (description.getText().isEmpty() && paymentId.getText().isEmpty()
                && !accountNumber.getText().isEmpty() && !amount.getText().isEmpty()) {
            payment = new Payment.PaymentBuilder(accountNumber.getText(),
                    Float.parseFloat(amount.getText())).setDate(LocalDateTime.now()).build();
            sdm.store(String.format("%s~%s~No description~no additional payment id~%s",
                    payment.getAccountId(), payment.getAmount(), payment.getTransactionDate().toString()));
            idm.store(String.format("%s~%s~No description~no additional payment id~%s~%s",
                    user.getId(), payment.getAmount(), payment.getTransactionDate().toString(), receiver.getId()));

            errorLabel.setText("");
        } else if (!description.getText().isEmpty() && paymentId.getText().isEmpty()) {
            payment = new Payment.PaymentBuilder(accountNumber.getText(),
                    Float.parseFloat(amount.getText())).setDescription(description.getText()).setDate(LocalDateTime.now()).build();
            sdm.store(String.format("%s~%s~%s~no additional payment~%s",
                    payment.getAccountId(), payment.getAmount(), payment.getDescription(), payment.getTransactionDate().toString()));
            idm.store(String.format("%s~%s~%s~no additional payment~%s~%s",
                    user.getId(), payment.getAmount(), payment.getDescription(), payment.getTransactionDate().toString(), receiver.getId()));
            errorLabel.setText("");

        } else if (description.getText().isEmpty() && !paymentId.getText().isEmpty()) {
            payment = new Payment.PaymentBuilder(accountNumber.getText(),
                    Float.parseFloat(amount.getText())).setDescription(description.getText())
                    .setPaymentId(paymentId.getText()).setDate(LocalDateTime.now()).build();
            sdm.store(String.format("%s~%s~No additional description~%s~%s",
                    payment.getAccountId(), payment.getAmount(), payment.getPaymentId(),
                    payment.getTransactionDate().toString()));
            idm.store(String.format("%s~%s~No additional description~%s~%s~%s",
                    user.getId(), payment.getAmount(), payment.getPaymentId(),
                    payment.getTransactionDate().toString(), receiver.getId()));
            errorLabel.setText("");

        } else if (!description.getText().isEmpty() && !paymentId.getText().isEmpty()
                && !accountNumber.getText().isEmpty() && !amount.getText().isEmpty()) {
            payment = new Payment.PaymentBuilder(accountNumber.getText(),
                    Float.parseFloat(amount.getText())).setDescription(description.getText())
                    .setPaymentId(paymentId.getText()).setDate(LocalDateTime.now()).build();
            sdm.store(String.format("%s~%s~%s~%s~%s",
                    payment.getAccountId(), payment.getAmount(), payment.getDescription(), payment.getPaymentId(),
                    payment.getTransactionDate().toString()));
            idm.store(String.format("%s~%s~%s~%s~%s~%s",
                    user.getId(), payment.getAmount(), payment.getDescription(), payment.getPaymentId(),
                    payment.getTransactionDate().toString(), receiver.getId()));
            errorLabel.setText("");
        } else {
            errorLabel.setTextFill(Color.RED);
            errorLabel.setText("Please fill all required fields!");
            return;
        }
        if (showConfirmationDialog(receiver)) {
            sdm.addPayment(payment);
        }
    }

    private void showInformationDialog(Payment payment) throws IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(mainPane.getScene().getWindow());
        alert.setTitle("Payment information");
        alert.setContentText("Receiver's account number:  " + payment.getAccountId() + "\n" +
                "Money amount:                    " + payment.getAmount() + "€" + "\n" +
                "Payment description:            " + payment.getDescription() + "\n" +
                "PaymentID:                           " + payment.getPaymentId() + "\n" +
                "Payment date:                    " + payment.getTransactionDate().format(DateFormat.getFormat()));
        alert.showAndWait();
    }

    private boolean showConfirmationDialog(User receiver) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initOwner(mainPane.getScene().getWindow());
        alert.setTitle("Payment confirmation");
        alert.setHeaderText("Are you sure about this transaction?");
        alert.setContentText("Receiver's account number:  " + receiver.getId() + "\n" +
                "Receiver's name:                   " + receiver.getUsername() + "\n" +
                "Transfer amount:                   " + amount.getText() + "€");

        Optional<ButtonType> result = alert.showAndWait();
        Clean.clearLabel(errorLabel, 5000);
        if (result.isPresent() && result.get() == ButtonType.OK) {
            processPayment(receiver);
            return true;
        } else {
            errorLabel.setTextFill(Color.YELLOWGREEN);
            errorLabel.setText("Money transfer cancelled");
            return false;
        }
    }

    // TODO COMMAND PATTERN IN ACTION

    private void processPayment(User receiver) throws IOException {

        CommandController controller = new CommandController();

        UserUpdater process1 = new UserUpdater(user, Float.parseFloat(amount.getText()));

        UserUpdater process2 = new UserUpdater(receiver,
                Float.parseFloat(amount.getText()));

        Command sendMoney = new SendMoneyCommand(process1);
        Command receiveMoney = new ReceiveMoneyCommand(process2);

        controller.setCommand(sendMoney);
        controller.execute();


        Subject userSubject = new UserSubject();
        new BalanceObserver(userSubject, sendMoney.getUpdatedUser());

        userSubject.updateState();

        controller.setCommand(receiveMoney);
        controller.execute();

        errorLabel.setTextFill(Color.GREEN);
        errorLabel.setText("Money sent!");

        // update balance

        initializeBalance();
        balanceEur.setValue(user.getCashBalance());
    }

    // TODO OBSERVER PATTERN IN USE

    public class BalanceObserver extends Observer {
        User updatedUser;

        public BalanceObserver(Subject subject, User updatedUser) {
            this.updatedUser = updatedUser;
            this.subject = subject;
            this.subject.register(this);
        }

        @Override
        public void update() {
            user.setCashBalance(updatedUser.getCashBalance());
        }
    }

    @FXML
    public void handleNavigationToAccount() throws IOException {
        // TODO: strategy design pattern
        // load income history data from files to memory

        ContextLoader loader = new ContextLoader(idm);
        loader.loadData();
        screenController.activate("account");
    }

    @FXML
    public void logout() {
        LogoutFacade.logout();
    }
}