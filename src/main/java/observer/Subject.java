package observer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class Subject {

    public abstract void updateState() throws IOException;

    final List<Observer> observers = new ArrayList<>();

    public void register(Observer obj) {
        observers.add(obj);
    }

    public void unregister(Observer obj) {
        observers.remove(obj);
    }

    public abstract void notifyObservers() throws IOException;
}
