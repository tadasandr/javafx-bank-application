package observer;

import java.io.IOException;

public class UserSubject extends Subject {

    @Override
    public void updateState() throws IOException {
        notifyObservers();
    }

    @Override
    public void register(Observer obj) {
        super.register(obj);
    }

    @Override
    public void unregister(Observer obj) {
        super.unregister(obj);
    }

    @Override
    public void notifyObservers() throws IOException {
        for (Observer observer : super.observers) {
            observer.update();
        }
    }
}
