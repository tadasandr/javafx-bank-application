package facade;

import controllers.ScreenController;
import singleton.UsersSingleton;
import utility.Clean;

public class LogoutFacade {
    final static UsersSingleton us = UsersSingleton.getInstance();
    final static ScreenController sc = ScreenController.getInstance();

    public static void logout() {
        us.unload();
        Clean.unloadData();
        Clean.unloadScreens();
        sc.activate("login");
    }
}
