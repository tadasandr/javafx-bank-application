package facade;

import command.UserUpdater;
import utility.Validator;

public class ChangePasswordFacade {

    private static UserUpdater userUpdater = new UserUpdater();
    private static Validator validator = new Validator();

    // TODO FACADE DESIGN PATTERN --------------------------------------------------------------------------------------

    public static boolean changePassword(String oldPassword, String newPassword, String repeatNewPassword) {
        if(!validator.doPasswordsMatch(newPassword, repeatNewPassword)) {
            return false;
        }
        if(!validator.isPasswordRegexCorrect(newPassword)) {
            return false;
        }
        if(!validator.checkIfPasswordCorrect(oldPassword)) {
            return false;
        }
        userUpdater.changePassword(newPassword);
        userUpdater.update();
        return true;
    }
}
