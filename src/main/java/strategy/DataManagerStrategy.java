package strategy;

import builder.Payment;
import javafx.collections.ObservableList;

import java.io.IOException;

public interface DataManagerStrategy {

    void load() throws IOException;
    void store(String text) throws IOException;
    void addPayment(Payment payment);
    void unload();
    ObservableList<Payment> getHistoryList();
}
