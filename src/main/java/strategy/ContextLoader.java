package strategy;

import java.io.IOException;

public class ContextLoader {
  
       private DataManagerStrategy strategy;
       
       public ContextLoader(DataManagerStrategy strategy){
          this.strategy = strategy;  
       }  
  
       public void loadData() throws IOException {
          strategy.load();
       }
}