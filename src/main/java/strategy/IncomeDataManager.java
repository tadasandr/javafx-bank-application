package strategy;

import builder.Payment;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import singleton.UsersSingleton;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class IncomeDataManager implements DataManagerStrategy {

    // TODO IMPLEMENTS STRATEGY AND SINGLETON --------------------------------------------------------------------------

    private static IncomeDataManager instance;
    private final ObservableList<Payment> incomeHistoryItems = FXCollections.observableArrayList();
    private static final UsersSingleton us = UsersSingleton.getInstance();
    private static String fileName;
    private String receiverFileName;

    private IncomeDataManager() { }

    @Override
    public void addPayment(Payment payment) {
        incomeHistoryItems.add(0, payment);
    }

    public ObservableList<Payment> getHistoryList() {
        return incomeHistoryItems;
    }

    public static synchronized IncomeDataManager getInstance() {
        if (instance == null) {
            instance = new IncomeDataManager();
            fileName = String.format("incomes%s.txt", us.getLoggedInUser().getId());
        }
        return instance;
    }

    @Override
    public void load() throws IOException {
        Path path = Paths.get(fileName);
        BufferedReader br = Files.newBufferedReader(path);

        String input;

        // LT545612345614-56.0-Uz pietus.-no additional payment id

        try {
            while ((input = br.readLine()) != null) {
                String[] itemPieces = input.split("~");

                String accountNumber = itemPieces[0];
                String amount = itemPieces[1];
                String description = itemPieces[2];
                String paymentId = itemPieces[3];
                String date = itemPieces[4];
                Payment payment = new Payment.PaymentBuilder(accountNumber, Float.parseFloat(amount)).setDescription(description)
                        .setPaymentId(paymentId).setDate(LocalDateTime.parse(date)).build();
                incomeHistoryItems.add(payment);
            }

        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    @Override
    public void store(String text) {
        receiverFileName = String.format("incomes%s.txt", text.split("~")[5]);
        try {
            BufferedWriter buffer = new BufferedWriter(new FileWriter(receiverFileName, true));
            buffer.write(text);
            buffer.write("\n");
            buffer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unload() {
        instance = null;
    }
}
