package strategy;

import builder.Payment;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import singleton.UsersSingleton;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class SpendingsDataManager implements DataManagerStrategy {

    // TODO IMPLEMENTS STRATEGY AND SINGLETON --------------------------------------------------------------------------

    private static SpendingsDataManager instance;
    private final ObservableList<Payment> spendingHistoryItems = FXCollections.observableArrayList();
    private static final UsersSingleton us = UsersSingleton.getInstance();
    private static String fileName;

    private SpendingsDataManager() { }

    @Override
    public void addPayment(Payment payment) {
        spendingHistoryItems.add(0, payment);
    }

    @Override
    public ObservableList<Payment> getHistoryList() {
        return spendingHistoryItems;
    }

    public static synchronized SpendingsDataManager getInstance() {
        if (instance == null) {
            instance = new SpendingsDataManager();
            fileName = String.format("spendings%s.txt", us.getLoggedInUser().getId());
        }
        return instance;
    }

    @Override
    public void load() throws IOException {
        Path path = Paths.get(fileName);
        BufferedReader br = Files.newBufferedReader(path);

        String input;

        // LT545612345614-56.0-Uz pietus.-no additional payment id

        try {
            while ((input = br.readLine()) != null) {
                String[] itemPieces = input.split("~");

                String accountNumber = itemPieces[0];
                String amount = itemPieces[1];
                String description = itemPieces[2];
                String paymentId = itemPieces[3];
                String date = itemPieces[4];
                Payment payment = new Payment.PaymentBuilder(accountNumber, Float.parseFloat(amount)).setDescription(description)
                        .setPaymentId(paymentId).setDate(LocalDateTime.parse(date)).build();
                spendingHistoryItems.add(payment);
            }

        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    @Override
    public void store(String text) throws IOException {
        try {
            BufferedWriter buffer = new BufferedWriter(new FileWriter(fileName, true));
            buffer.write(text);
            buffer.write("\n");
            buffer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void unload() {
        instance = null;
    }
}
