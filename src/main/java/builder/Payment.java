package builder;

// TODO BUILDER PATTERN ------------------------------------------------------------------------------------------------

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Payment {

    // required fields

    private String accountId;
    private float amount;
    private LocalDateTime transactionDate;

    // optional fields

    private String description;
    private String paymentId;

    protected Payment() {}

    public String getAccountId() {
        return accountId;
    }

    public float getAmount() {
        return amount;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public String getDescription() {
        return description;
    }

    public String getPaymentId() {
        return paymentId;
    }

    private Payment(PaymentBuilder builder) {
        this.accountId = builder.accountId;
        this.amount = builder.amount;
        this.description = builder.description;
        this.paymentId = builder.paymentId;
        this.transactionDate = builder.transactionDate;
    }

    public static class PaymentBuilder implements Builder<Payment> {

        // required fields

        private String accountId;
        private float amount;
        private LocalDateTime transactionDate;

        // optional fields

        private String description;
        private String paymentId;

        public PaymentBuilder(String accountId, float amount) {
            this.accountId = accountId;
            this.amount = amount;
        }


        public PaymentBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public PaymentBuilder setPaymentId(String paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public PaymentBuilder setDate(LocalDateTime date) {
            this.transactionDate = date;
            return this;
        }

        @Override
        public Payment build() {
            return new Payment(this);
        }
    }
}
