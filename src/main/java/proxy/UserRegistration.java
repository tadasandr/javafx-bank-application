package proxy;

import singleton.UsersSingleton;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class UserRegistration implements Register {

    private static String filename = "Users.txt";
    private final UsersSingleton us = UsersSingleton.getInstance();

    @Override
    public boolean register(String username, String password) {
        try {
            BufferedWriter buffer = new BufferedWriter(new FileWriter(filename, true));
            String randomId = us.generateRandomId();
            buffer.write(randomId + "~" + username + "~" + password + "~" + 150f);
            buffer.write("\n");
            createFiles(randomId);
            buffer.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void createFiles(String id) {
        try {
            File spendings = new File("spendings" + id + ".txt");
            File incomes = new File("incomes" + id + ".txt");
            if (spendings.createNewFile() && incomes.createNewFile()) {
                System.out.println("Files successfully created");
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
