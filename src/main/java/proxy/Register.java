package proxy;

public interface Register {
    boolean register(String username, String password);
}
