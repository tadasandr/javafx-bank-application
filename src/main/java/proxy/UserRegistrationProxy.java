package proxy;

import utility.Validator;

public class UserRegistrationProxy implements Register {

    // TODO THIS IS A USER REGISTRATION DATA PROTECTION PROXY

    private final Register registration = new UserRegistration();
    private final Validator validator = new Validator();

    @Override
    public boolean register(String username, String password) {
        if (validator.isPasswordRegexCorrect(password)) {
            if(registration.register(username, password)) {
                return true;
            }
        }
        return false;
    }
}
