package utility;

import singleton.UsersSingleton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    final UsersSingleton us = UsersSingleton.getInstance();

    public boolean isPasswordRegexCorrect(String password) {
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,20}$";

        Pattern p = Pattern.compile(regex);

        if (password == null) {
            return false;
        }

        Matcher m = p.matcher(password);

        return m.matches();
    }

    public boolean checkIfPasswordCorrect(String password) {
        return password.equals(us.getLoggedInUser().getPassword());
    }

    public boolean doPasswordsMatch(String p1, String p2) {
        return p1.equals(p2);
    }
}
