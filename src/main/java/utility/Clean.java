package utility;

import controllers.ScreenController;
import javafx.application.Platform;
import javafx.scene.control.Label;
import strategy.DataManagerStrategy;
import strategy.IncomeDataManager;
import strategy.SpendingsDataManager;

import java.util.ArrayList;
import java.util.List;

public class Clean {

    final static DataManagerStrategy sdm = SpendingsDataManager.getInstance();
    final static DataManagerStrategy idm = IncomeDataManager.getInstance();
    final static ScreenController sc = ScreenController.getInstance();

    public static void clearLabel(Label label, long timeMillis) {
            Runnable clearErrorLabel = () -> {
        try {
            Thread.sleep(timeMillis);
            Platform.runLater(() -> {
                label.setText("");
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    };
        new Thread(clearErrorLabel).start();
    }

    public static void unloadData() {
        List<DataManagerStrategy> loaders = new ArrayList<>() {{
            add(idm);
            add(sdm);
        }};
        for (DataManagerStrategy loader : loaders) {
            loader.unload();
            System.out.println(loader.getHistoryList() + " unloaded");
        }
    }

    public static void unloadScreens() {
        sc.removeScreen("dashboard");
        sc.removeScreen("account");
    }
}
