package utility;

import java.time.format.DateTimeFormatter;

public class DateFormat {
    static DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static DateTimeFormatter getFormat() {
        return format;
    }
}
