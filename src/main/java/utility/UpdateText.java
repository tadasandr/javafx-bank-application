package utility;

import javafx.scene.control.TextField;
import singleton.UsersSingleton;

public class UpdateText {

    static UsersSingleton us = UsersSingleton.getInstance();

    public static void initializeBalance(TextField balance) {
        balance.setText(String.format("Current balance: %s €", us.getLoggedInUser().getCashBalance()));
    }
}
