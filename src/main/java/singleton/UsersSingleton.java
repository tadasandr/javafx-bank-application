package singleton;

import datamodel.User;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Random;


public class UsersSingleton {
    private static UsersSingleton instance;
    private User loadedInUser;
    private static String filename = "Users.txt";

    public static synchronized UsersSingleton getInstance() {
        if (instance == null) {
            instance = new UsersSingleton();
        }
        return instance;
    }

    private UsersSingleton() { }

    public boolean login(String username, String password) throws IOException {
        User user = find(username, password);
        if(user != null) {
            loadedInUser = user;
            return true;
        }
        return false;
    }

    public User find(String username, String password) throws IOException {
        Path path = Paths.get(filename);
        BufferedReader br = Files.newBufferedReader(path);

        String input;
        User user = null;

        try {
            while ((input = br.readLine()) != null) {
                String[] itemPieces = input.split("~");

                String id = itemPieces[0];
                String usernameFile = itemPieces[1];
                String passwordFile = itemPieces[2];
                String balance = itemPieces[3];

                if(usernameFile.equals(username) && passwordFile.equals(password)) {
                    user = new User(id, usernameFile, passwordFile, Float.parseFloat(balance));
                    break;
                }
            }

        } finally {
            if (br != null) {
                br.close();
            }
        }
        return user;
    }

    public User findById(String id) throws IOException {
        Path path = Paths.get(filename);
        BufferedReader br = Files.newBufferedReader(path);

        String input;
        User user = null;

        try {
            while ((input = br.readLine()) != null) {
                String[] itemPieces = input.split("~");

                String accountId = itemPieces[0];
                String usernameFile = itemPieces[1];
                String passwordFile = itemPieces[2];
                String balance = itemPieces[3];

                if(id.equals(accountId)) {
                    user = new User(id, usernameFile, passwordFile, Float.parseFloat(balance));
                    break;
                }
            }

        } finally {
            if (br != null) {
                br.close();
            }
        }
        if(user != null) {
            return user;
        }
        return null;
    }

    public String generateRandomId() {
        Random random = new Random();
        return String.format((Locale)null,
                "LT52%02d%04d%04d%04d",
                random.nextInt(1000),
                random.nextInt(10000),
                random.nextInt(10000),
                random.nextInt(10000));
    }

    public void setLoadedInUser(User loadedInUser) {
        this.loadedInUser = loadedInUser;
    }

    public User getLoggedInUser() {
        return loadedInUser;
    }

    public void unload() {
        instance = null;
    }
}