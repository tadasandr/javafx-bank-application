package adapter;

public interface Currency {
    double getValue();
    void setValue(double value);
    String getCurrencySymbol();
}
