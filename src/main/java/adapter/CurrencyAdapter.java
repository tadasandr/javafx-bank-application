package adapter;

public interface CurrencyAdapter {
    double getValue();
    String getName();
    String getCurrencySymbol();
}
