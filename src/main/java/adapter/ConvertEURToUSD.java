package adapter;

public class ConvertEURToUSD implements CurrencyAdapter {

    Currency currency;

    public ConvertEURToUSD(Currency currency) {
        this.currency = currency;
    }

    @Override
    public double getValue() {
        return currency.getValue() * 1.06;
    }

    @Override
    public String getName() {
        return "USD";
    }

    @Override
    public String getCurrencySymbol() {
        return "$";
    }
}
