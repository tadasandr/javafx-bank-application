package adapter;

public class ConvertEURToEUR implements CurrencyAdapter {

    Currency currency;

    public ConvertEURToEUR(Currency currency) {
        this.currency = currency;
    }

    @Override
    public double getValue() {
        return currency.getValue();
    }

    @Override
    public String getName() {
        return "EUR";
    }

    @Override
    public String getCurrencySymbol() {
        return "€";
    }
}
