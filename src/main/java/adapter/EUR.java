package adapter;

public class EUR implements Currency {
    private final String currencySymbol = "€";
    private double value;

    public EUR(double value) {
        this.value = value;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String getCurrencySymbol() {
        return currencySymbol;
    }
}
