package adapter;

public class ConvertEURToZL implements CurrencyAdapter {

    Currency currency;

    public ConvertEURToZL(Currency currency) {
        this.currency = currency;
    }

    @Override
    public double getValue() {
        return currency.getValue() * 4.67;
    }

    @Override
    public String getName() {
        return "ZL";
    }

    @Override
    public String getCurrencySymbol() {
        return "zł";
    }
}
