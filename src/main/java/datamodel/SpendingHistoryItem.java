package datamodel;

import java.time.LocalDate;

public class SpendingHistoryItem {
    private String paidToId;
    private String paidToName;
    private float paidAmount;
    private String description;
    private LocalDate dateOfAction;

    public SpendingHistoryItem(String paidToId, String paidToName, float paidAmount,
                               String description) {
        this.paidToId = paidToId;
        this.paidToName = paidToName;
        this.paidAmount = paidAmount;
        this.description = description;
        this.dateOfAction = LocalDate.now();
    }

    public String getPaidToId() {
        return paidToId;
    }

    public void setPaidToId(String paidToId) {
        this.paidToId = paidToId;
    }

    public String getPaidToName() {
        return paidToName;
    }

    public void setPaidToName(String paidToName) {
        this.paidToName = paidToName;
    }

    public float getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(float paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateOfAction() {
        return dateOfAction;
    }

    public void setDateOfAction(LocalDate dateOfAction) {
        this.dateOfAction = dateOfAction;
    }

    @Override
    public String toString() {
        return "SpendingHistoryItem{" +
                "paidToId='" + paidToId + '\'' +
                ", paidToName='" + paidToName + '\'' +
                ", paidAmount=" + paidAmount +
                ", description='" + description + '\'' +
                ", dateOfAction=" + dateOfAction +
                '}';
    }
}
