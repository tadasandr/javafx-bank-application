package datamodel;

import prototype.UserPrototype;

public class User implements UserPrototype {
    private String id;

    private String username;
    private String password;
    private float cashBalance;

    public User(String id, String username, String password, float cashBalance) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.cashBalance = cashBalance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public float getCashBalance() {
        return cashBalance;
    }

    public void setCashBalance(float cashBalance) {
        this.cashBalance = cashBalance;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", cashBalance=" + cashBalance +
                '}';
    }

    // TODO: PROTOTYPE IMPLEMENTATION ----------------------------------------------------------------------------------

    @Override
    public UserPrototype getClone() {
        return new User(this.id, this.username, this.password, this.cashBalance);
    }
}
