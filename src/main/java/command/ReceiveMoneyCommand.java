package command;

import datamodel.User;

public class ReceiveMoneyCommand implements Command {

    private UserUpdater userUpdater;

    public ReceiveMoneyCommand(UserUpdater userUpdater) {
        this.userUpdater = userUpdater;
    }

    @Override
    public void execute() {
        userUpdater.increaseMoney();
        userUpdater.update();
    }

    @Override
    public User getUpdatedUser() {
        return null;
    }
}
