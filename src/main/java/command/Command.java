package command;

import datamodel.User;
import observer.Subject;

public interface Command {
    void execute();
    User getUpdatedUser();
}
