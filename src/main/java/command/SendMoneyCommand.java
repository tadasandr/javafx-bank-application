package command;

import datamodel.User;

public class SendMoneyCommand implements Command {

    private UserUpdater userUpdater;
    private User updatedUser;

    public SendMoneyCommand(UserUpdater userUpdater) {
        this.userUpdater = userUpdater;
    }

    @Override
    public void execute() {
        updatedUser = userUpdater.decreaseMoney();
        userUpdater.update();
    }

    public User getUpdatedUser() {
        return updatedUser;
    }
}
