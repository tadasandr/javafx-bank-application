package command;

public class CommandController {

    Command slot;

    public void setCommand(Command command)
    {
        slot = command;
    }

    public void execute()
    {
        slot.execute();
    }
}
