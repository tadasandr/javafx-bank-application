package command;

import datamodel.User;
import singleton.UsersSingleton;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;


// HOLDS THREE COMMANDS FOR USE

public class UserUpdater {

    private final static String filename = "Users.txt";
    private final UsersSingleton us = UsersSingleton.getInstance();
    private User user;
    private User userUpdated;
    private float amount;

    // TODO: PROTOTYPE IMPLEMENTATION

    public UserUpdater(User u1, float amount) {
        this.user = u1;
        this.userUpdated = (User) this.user.getClone();
        this.amount = amount;
    }

    // TODO: PROTOTYPE IMPLEMENTATION

    public UserUpdater() {
        this.user = us.getLoggedInUser();
        this.userUpdated = (User) this.user.getClone();
    }

    public User increaseMoney() {
        userUpdated.setCashBalance(userUpdated.getCashBalance() + amount);
        return userUpdated;
    }

    public User decreaseMoney() {
        userUpdated.setCashBalance(userUpdated.getCashBalance() - amount);
        return userUpdated;
    }

    public User changePassword(String password) {
        userUpdated.setPassword(password);
        return userUpdated;
    }


    public void update() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder inputBuffer = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                inputBuffer.append(line);
                inputBuffer.append('\n');
            }
            br.close();
            String inputStr = inputBuffer.toString();

            System.out.println("Changing from " + user + " to " + userUpdated);;

            inputStr = inputStr.replace(String.format("%s~%s~%s~%s", user.getId(), user.getUsername(),
                    user.getPassword(), user.getCashBalance()), String.format("%s~%s~%s~%s", userUpdated.getId(),
                    userUpdated.getUsername(), userUpdated.getPassword(), userUpdated.getCashBalance()));

            System.out.println(inputStr);

            FileOutputStream fileOut = new FileOutputStream(filename);
            fileOut.write(inputStr.getBytes());
            fileOut.close();

        } catch (Exception e) {
            System.out.println("Problem reading file.");
        }
    }

    public User getUserUpdated() {
        return userUpdated;
    }
}
