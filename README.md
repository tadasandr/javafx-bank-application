<h2>JavaFX banking application simulator<h2>
<br/>
<p>Banking application using Java and JavaFX library.</p>
<p>This application is working entirely on file system. Features: </p>
<ul>
<li>Implementation of 7 design patterns:
<ul>
<li>
Adapter
</li>
<li>
Builder
</li>
<li>
Command
</li>
<li>
Facade
</li>
<li>
Observer
</li>
<li>
Prototype
</li>
<li>
Proxy
</li>
<li>
Singleton
</li>
<li>
Strategy
</li>
</ul>
 </li>
<li>Authentication</li>
<img src="https://gitlab.com/tadasandr/javafx-bank-application/-/raw/main/assets/bank1.png"/>
<li>Sending money to other accounts and displaying history</li>
<img src="https://gitlab.com/tadasandr/javafx-bank-application/-/raw/main/assets/bank2.png"/>
<img src="https://gitlab.com/tadasandr/javafx-bank-application/-/raw/main/assets/bank5.png"/>
<li>Receiving money and displaying history</li>
<img src="https://gitlab.com/tadasandr/javafx-bank-application/-/raw/main/assets/bank4.png"/>
<li>Validation on funds/authentication/valid account numbers...</li>
<li>Password reset</li>
<li>
Pop-up windows
</li>
<li>
Context menus
</li>
<li>
Money conversion using Adapters
</li>
<img src="https://gitlab.com/tadasandr/javafx-bank-application/-/raw/main/assets/bank6.png"/>
</ul>